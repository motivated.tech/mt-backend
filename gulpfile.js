var gulp = require('gulp')
var server = require('gulp-develop-server')

gulp.task('default', ['server:start'], function() {
})

// run server
gulp.task( 'server:start', function() {
	server.listen( { path: './api/server.js' } )
	gulp.watch( [ './*.js', './api/*.js', './api/models/*.js', './api/routes/*.js', './api/controllers/*.js', './api/utils/*.js' ], server.restart )

})