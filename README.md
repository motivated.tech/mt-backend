# mt-backend Documentation

## Schemas

### UserSchema:
`{
userId: { type: String, unique: true, required: true },
username: { type: String, unique: true, required: true },
password: { type: String, required: true },
name: { type: String },
email: { type: String },
permissions: { type: Array }
}`

### CourseSchema:
`{
courseId: { type: String, required: true, unique: true },
name: { type: String, required: true },
description: { type: String },
groups: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Group' }],
}`

### GroupSchema
`{
groupId: { type: String, required: true, unique: true },
name: { type: String},
description: { type: String },
course: { type: mongoose.Schema.Types.ObjectId, ref: 'Course', required: true },
chats: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Chat' }],
}`

### ChatSchema
`{
chatId: { type: String, required: true, unique: true },
messages: { type: Array },
group: { type: mongoose.Schema.Types.ObjectId, ref: 'Group' }
}`

## API Calls


#### User
`GET /user`

List all users if requested by a user with admin permission.

**TODO:** list user friends

`Get /user/:user_id`

Retrieve `:user_id`. User can only retrieve itself, admin any

`POST /user`

Add new user. Admin rights required

`PUT /user/:user_id`

Edit `:user_id`

`DELETE /user/:user_id`

Delete `:user_id`

#### Course
`GET /course`

Retrieve courses the user is member of. Admin gets all the courses.

`GET /course/:course_id`

Retrieve :course_id

`POST /course`

New course

`PUT /course/:course_id`

Update :course_id

`DELETE /course/:course_id`

Delete :course_id

### Group

`GET /group`

Retrieve list of all groups

`POST /group`

New group and new chat for the group

### Chat

`GET /chat`

List all chats

*TODO:*
`GET /chat/:chat_id`

Get :chat_id with messages

`POST /chat`

New chat for group `req.body.group`
