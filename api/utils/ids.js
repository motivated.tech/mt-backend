var IDs = require('../models/ids')
const version = 1000;

module.exports = {
	getNewUserId() {
		return new Promise((resolve, reject) => {
			IDs.findOne({ version }, 'userId')
			.catch((err) => { console.error(err) })
			.then((doc) => {
				var newUserId = doc.userId + 1

				IDs.findOneAndUpdate({ version }, { userId: newUserId }, {new: true})
				.then((doc) => { resolve(doc.userId) })
				.catch((err) => { reject(err) })
			})
		})
	},

getNewUsersIds(number) {
		return new Promise((resolve, reject) => {
			IDs.findOne({ version }, 'userId')
			.catch((err) => { console.error(err) })
			.then((doc) => {
				var newUserId = doc.userId + number

				IDs.findOneAndUpdate({ version }, { userId: newUserId }, {new: true})
				.then((doc) => { resolve(doc.userId) })
				.catch((err) => { reject(err) })
			})
		})
	},

	getNewCourseId() {
		return new Promise((resolve, reject) => {
			IDs.findOne({ version }, 'courseId')
			.catch((err) => { console.error(err) })
			.then((doc) => {
				var newCourseId = doc.courseId + 1
				IDs.findOneAndUpdate({ version }, { courseId: newCourseId }, {new: true})
				.catch((err) => { reject(err) })
				.then((doc) => { resolve(doc.courseId) })
			})
		})
	},

	getNewGroupId() {
		return new Promise((resolve, reject) => {
			IDs.findOne({ version }, 'groupId')
			.catch((err) => { console.error(err) })
			.then((doc) => {
				var newGroupId = doc.groupId + 1
				IDs.findOneAndUpdate({ version }, { groupId: newGroupId }, {new: true})
				.catch((err) => { reject(err) })
				.then((doc) => { resolve(doc.groupId) })
			})
		})

	},

	getNewChatId() {
		return new Promise((resolve, reject) => {
			IDs.findOne({ version }, 'chatId')
			.catch((err) => { console.error(err) })
			.then((doc) => {
				var newChatId = doc.chatId + 1
				IDs.findOneAndUpdate({ version }, { chatId: newChatId }, {new: true})
				.catch((err) => { reject(err) })
				.then((doc) => { resolve(doc.chatId) })
			})
		})


	}
}
