const jdenticon = require("jdenticon")
const fs = require("fs")
const md5 = require("md5")

module.exports = (username, size) => {
	const svg = jdenticon.toSvg(md5(username), size)
	fs.writeFile(`./user_pics/${username}.svg`, svg, (err) => {
		if (err) throw err
	})
}
