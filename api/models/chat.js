var mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId

var ChatSchema = new mongoose.Schema({
	id: { type: String, required: true, unique: true },
	messages: { type: Array },
	group: { type: ObjectId, ref: 'Group' }
})

module.exports = mongoose.model('Chat', ChatSchema)
