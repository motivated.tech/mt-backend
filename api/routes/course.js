var router = require('express').Router()
var course = require('../controllers/course')
var guard = require('express-jwt-permissions')()

// GET all courses
router.get('/', (req, res) => {
	course.getAllCourses()
	.catch((err) => res.status(500).send(err))
	.then((courses) => res.json(courses))
})

// Get single course by id
router.get('/:course_id', (req, res) => {
	course.getOneCourse(req.params.course_id)
	.catch((err) => res.status(500).send(err))
	.then((course) => {
		course._id = undefined
		res.json(course)
	})
})

// New course
router.post('/new_course', guard.check('admin'), (req, res) => {
	course.newCourse(req.body)
	.catch((err) => res.status(500).send(err))
	.then((course) => res.json(course))
})

router.post('/:course_id/new_group', guard.check('admin'), (req, res) => {
	course.addGroupToCourse(req.params.course_id)
	.catch((err) => res.status(500).send(err))
	.then((course) => res.json(course))
})

// Update
router.put('/:course_id', guard.check('admin'), (req, res) => {
	course.updateCourse(req.params.course_id, req.body)
	.catch((err) => res.status(500).send(err))
	.then((course) => res.json(course))
})

// Delete
router.delete('/:course_id', guard.check('admin'), (req, res) => {
	course.deleteCourse(req.params.course_id)
	.catch((err) => res.status(500).send(err))
	.then((val) => res.json(val))
})

module.exports = router