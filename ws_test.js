var WebSocketServer = require('uws').Server;
var wss = new WebSocketServer({ port: 3000, path: 'test' });

function onMessage(message) {
    console.log('received: ' + message);
}

var ws = null

wss.on('connection', function(ws1) {
	ws = ws1
	console.log('connected')
    ws1.on('message', onMessage);
    ws1.send('something');
});
